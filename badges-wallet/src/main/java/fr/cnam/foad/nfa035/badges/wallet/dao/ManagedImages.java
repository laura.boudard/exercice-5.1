package fr.cnam.foad.nfa035.badges.wallet.dao;

import org.springframework.http.MediaType;

/**
 * Enumeration permettant de prendre en charge plusieurs formats de mimeType
 */
public enum ManagedImages {

   jpeg("image/jpeg" ), jpg("image/jpeg"), png("image/png"), gif("image/gif");

   private String mimeType;

    /**
     * Constructeur
     * @param mimeType
     */
   ManagedImages(String mimeType) {
     this.mimeType = mimeType;
   }

    /**
     * retourne le format de l'image
     * @return
     */
   public String getMimeType() {
     return mimeType;
   }

}
