package fr.cnam.foad.nfa035.badges.wallet.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import fr.cnam.foad.nfa035.badges.wallet.dao.ManagedImages;
import org.springframework.util.MimeType;

import java.util.Objects;

/**
 * POJO model représentant les Métadonnées d'un Badge Digital
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DigitalBadgeMetadata implements Comparable<DigitalBadgeMetadata>{

    private int badgeId;
    private long walletPosition;
    private long imageSize;
    private String imageType;

    /**
     * Constructeur
     *
     * @param badgeId
     * @param imageSize
     */
    public DigitalBadgeMetadata(int badgeId, long walletPosition, long imageSize) {
        this.badgeId = badgeId;
        this.walletPosition = walletPosition;
        this.imageSize = imageSize;

    }

    /**
     * Minimal contructor
     *
     * @param badgeId
     * @param imageSize
     */
    public DigitalBadgeMetadata(int badgeId, long imageSize) {
        this.badgeId = badgeId;
        this.imageSize = imageSize;

    }

    /**
     * Constructeur par défaut
     */
    public DigitalBadgeMetadata() {
    }

    /**
     * Getter du badgeId
     * @return l'id du badge
     */
    public int getBadgeId() {
        return badgeId;
    }

    /**
     * Setter du badgeId
     * @param badgeId
     */
    public void setBadgeId(int badgeId) {
        this.badgeId = badgeId;
    }

    /**
     * Getter de la taille de l'image
     * @return long la taille de l'image
     */
    public long getImageSize() {
        return imageSize;
    }

    /**
     * Setter de l'imageSize
     * @param imageSize
     */
    public void setImageSize(long imageSize) {
        this.imageSize = imageSize;
    }

    /**
     * Getter de la position du badge dans le wallet
     * @return long la position du badge dans le wallet
     */
    public long getWalletPosition() {
        return walletPosition;
    }

    /**
     * Setter de la position du badge dans le wallet
     * @param walletPosition
     */
    public void setWalletPosition(long walletPosition) {
        this.walletPosition = walletPosition;
    }

    /**
     * Getter du type Mime
     * @return String le type d'image
     */
    public String getImageType() {
        return imageType;
    }

    /**
     * Setter du type Mime
     * @param imageType  le type d'image
     */
    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    /**
     * {@inheritDoc}
     *
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigitalBadgeMetadata that = (DigitalBadgeMetadata) o;
        return badgeId == that.badgeId && walletPosition == that.walletPosition && imageSize == that.imageSize;
    }

    /**
     * {@inheritDoc}
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(badgeId, walletPosition, imageSize);
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public String toString() {
        return "DigitalBadgeMetadata{" +
                "badgeId=" + badgeId +
                ", imageSize=" + imageSize +
                '}';
    }


    @Override
    public int compareTo(DigitalBadgeMetadata o) {
        return this.getBadgeId() - o.getBadgeId();
    }

}
