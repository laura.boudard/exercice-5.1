package fr.cnam.foad.nfa035.badges.service.impl;

import fr.cnam.foad.nfa035.badges.service.BadgesWalletRestService;

import fr.cnam.foad.nfa035.badges.wallet.dao.ManagedImages;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.IOException;
import java.util.Set;
import java.util.SortedMap;

/**
 * Commentez-moi
 */
@RestController
public class BadgesWalletRestServiceImpl implements BadgesWalletRestService {

    @Qualifier("jsonBadge")
    @Autowired
    JSONBadgeWalletDAO jsonBadgeDao;

    ManagedImages managedImages;


    /**
     * {@inheritDoc}
     *
     * @param badge le badge à écrire, ou plutôt ses métadonnées
     * @param file le fichier image du badge à écrire
     * @return ResponseEntity la réponse REST toujours
     */
    @Override
    public ResponseEntity putBadge(@RequestPart DigitalBadge badge, @RequestPart MultipartFile file) {
        try {
            SortedMap<DigitalBadge, DigitalBadgeMetadata> badges = jsonBadgeDao.getWalletMetadataMap();
            if (badges.containsKey(badge)){
                // logique de suppression
            }
            jsonBadgeDao.addBadge(badge, file.getInputStream());
            return ResponseEntity.ok().build();
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param badge le badge à lire
     * @return ResponseEntity la réponse REST toujours
     */
    @Override
    public ResponseEntity<StreamingResponseBody> readBadge(@RequestBody DigitalBadge badge, @RequestParam boolean attachment) {

        StreamingResponseBody responseBody = outputStream -> {
            jsonBadgeDao.getBadgeFromMetadata(outputStream, badge);
        };
        // Il va falloir ajouter le MIME-TYPE de l'image dans les métadonnées du badge à un moment
        String imageType = badge.getMetadata().getImageType();
        MediaType mediaType = MediaType.valueOf(imageType);
        attachment = false;

        // En attendant, on prévoit png par défaut
        String fileName= badge.getSerial() + "." + imageType;
        HttpHeaders responseHeaders = new HttpHeaders();
        if (attachment == true) {

            responseHeaders.add("content-disposition", "attachment; filename=" + fileName);
        }

        return ResponseEntity.ok().headers(responseHeaders).contentType(mediaType)
                .body(responseBody);

    }




    /**
     * {@inheritDoc}
     *
     * @return ResponseEntity la réponse REST toujours
     */
    @Override
    public ResponseEntity<Set<DigitalBadge>> getMetadata() {
        try {
            return ResponseEntity.ok().body(jsonBadgeDao.getWalletMetadata());
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param badge le badge à supprimer
     * @return ResponseEntity la réponse REST toujours
     */
    @Override
    public ResponseEntity deleteBadge(@RequestBody DigitalBadge badge) {
        try {
            SortedMap<DigitalBadge, DigitalBadgeMetadata> badges = jsonBadgeDao.getWalletMetadataMap();
            if (badges.containsKey(badge)){
                // logique de suppression à faire
            }
            else {
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok().build();
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }
    }
}
